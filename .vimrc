" Create 2017/11/28
" Update 2017/11/28

"Vim editor color on
syntax on

"Vim syntax colors
colorscheme molokai

"Ruler
set ruler

"Line number
set number

"Terminal acceleration
set ttyfast

"Use 256 color display at terminal
set t_Co=256

"Automatically reload when the contents change
set autoread

"Highlight search results
set hlsearch

"Always show status line
set laststatus=2