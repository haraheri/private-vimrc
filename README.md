# .vimrc #

My private vimrc ripository

## how to use ##

### color theme monokai ###

* monokai download 

http://www.vim.org/scripts/script.php?script_id=2340

* move download file

```
$ cd ~
$ mkdir -p ~/.vim/colors
$ sudo mv ~/Downloads/molokai.vim ~/.vim/colors/molokai.vim

$ cp /usr/share/vim/vimrc ~/.vim
```

### clone Repository ###

```
$ mkdir vimrc-repo
$ cd vimrc-repo

$ git clone https://haraheri@bitbucket.org/haraheri/private-vimrc.git

$ cp -ap .vimrc ~/.vimrc
```